# 20165309 2017-2018-2 《Java程序设计》第五周学习总结

## 教材学习内容总结

### 第7章 内部类与异常类

**内部类**

- 内部类的类体中不可以声明类变量和类方法。外嵌类的类体中可以用内部类声明对象，作为外嵌类的成员。
- 外嵌类的成员变量在内部类中仍然有效，内部类中的方法也可以调用外嵌类中的方法。
- 内部类仅供它的外嵌类使用，二者在编译时生成两个.class文件。  
- 非内部类不可以是static类。

**匿名类** P164

- 和子类有关的匿名类
- 和接口有关的匿名类

**异常类** P167

- 用try-catch语句处理异常
    - try部分放可能出现的异常操作
    - catch部分放发生异常后的处理
    - 带finally子语句的try-catch语句:
        ``` 
        try{}
        catch(ExceptionSubClass e){ }
        finally{} //都会被执行
        ```
- 用throws关键字声明异常，用throw关键字抛出该异常对象。

**断言**
```
assert booleanExpression;
```
```
assert booleanExpression:messageException;
```

### 第10章 输入、输出流

**File类**

- File类的对象主要用来获取文件本身的一些信息，不涉及对文件的读写操作。
    - 创建一个File对象的构造方法 P282
- 获取文件的属性 P282
- 目录
    - 创建目录```public boolean mkdir()```
    - 列出目录中的文件 P283
    - 文件的创建与删除
    - 运行可执行文件
    
**文件字节输入流**

- 设定输入流的源->创建指向源的输入流(FileInputStream流)->让输入流读取源中的数据(调用从父类继承的read方法顺序读取文件)->关闭输入流

**文件字节输出流**

- 在try块部分创建FileOutputStream流
- 调用write方法顺序地向目的地写入内容，直到流被关闭
 
**缓冲流**

- 构造方法：
    ```
    BufferedReader(Reader in); 
    BufferedWriter (Writer out); 
    ```
- 读写文件的方法：
    ```
    readLine() //读取文本行
    write(String s,int off,int len) //把字符串s写到文件中
    newLine(); //向文件写入一个回行符
    ```
**随机流**

- RandomAccessFile类创建的流的指向既可以作为源也可以作为目的地。
- 相关方法：
    ```
    seek(long a)  //定位RandomAccessFile流的读写位置 
    getFilePointer() //获取流的当前读写位置  
    ```
    
**数组流**

- 字节数组流   ByteArrayInputStream、ByteArrayOutputStream
- 字符数组流 CharArrayReader和CharArrayWriter

**数据流**

DataInputStream和DataOutputStream

**对象流**

- 构造方法：
    ```
    ObjectInputStream(InputStream in)
    ObjectOutputStream(OutputStream out) 
    ```
- 相关方法：
    ```
    writeObject(Object obj) //将一个对象obj写入到一个文件
    readObject() //读取一个对象到程序中  
    ```
**序列化与对象克隆** P302

**使用Scanner解析文件**

- 使用默认分隔标记解析文件
- 使用正则表达式作为分隔标记解析文件

**文件对话框**

- 用JFileChooser()创建初始不可见的有模式的文件对话框。
- 文件对话框调用下述方法：

  ```showSaveDialog(Component a);```

  ```showOpenDialog(Component a);```

**带进度条的输入流**

```ProgressMonitorInputStream(Conmponent c,String s,InputStream);```

**文件锁** P309

## 教材学习中的问题和解决过程

- 问题：怎么判断该把什么放进try部分里面？
- 问题解决方案：https://www.cnblogs.com/yn-yinian/p/7784116.html

## 代码调试中的问题和解决过程

- 问题1：

    ![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331193500508-2138454003.png)

- 问题1解决方案：最后少打了一个“}”。
- 问题2：

    ![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331193713562-262779625.png)

- 问题2解决方案：原因是自己在码云的网页上创建了一个文件夹，git pull一下就好了。
    
    ![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331193834587-707919541.png)

- 问题3：教材上的代码Example10_8无法编译
- 问题3解决方案：应该是书上的```import java.io.**;```错了，多了一个```*```。

## [代码托管](https://gitee.com/BESTI-IS-JAVA-2018/20165309)

![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331001702504-65218496.png)

## 上周考试错题总结

-  
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002704501-1137757084.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002712112-1464792591.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002720718-1130387049.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002733467-2027409174.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002745752-574598253.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002752812-558862404.png)

- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002800048-1760047306.png)


- 
![](https://images2018.cnblogs.com/blog/1271290/201803/1271290-20180331002813076-1979180153.png)

本题解析：m是局部变量，没有默认值，所以在没有初始化的前提下使用会报错。

## 学习感悟

感觉进入到第10章后代码难度和复杂度突然变大了，自己在理解方面存在着困难...慢慢学吧。