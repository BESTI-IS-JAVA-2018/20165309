public class Judgement {
    private static int count = 0;
    public static void judge(boolean right, String num) {
        if (right) {
            System.out.println("回答正确！");
            count++;
        } else {
            System.out.println("回答错误！");
            System.out.println("正确的结果为：" + num);
        }
    }
    public static int getTrues(){
        return count;
    }
}

