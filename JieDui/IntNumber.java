import java.util.Random;
public class IntNumber { //整数加减乘除
    private int A;
    public IntNumber(int A){
        this.A = A;
    }
    public int add(IntNumber op1){ //加法
        int result = A + op1.A;
        System.out.print(A + " + " + op1.A + "=");
        return result;
    }
    public int subtract(IntNumber op1){  //减法
        int result = A - op1.A;
        System.out.print(A + " - " + op1.A + "=");
        return result;
    }
    public int multiply(IntNumber op1){ //乘法
        int result = A * op1.A;
        System.out.print(A + " * " + op1.A + "=");
        return result;
    }
    public String divide(Fraction op1){ //除法
        System.out.print(op1.getNumerator() + " / " + op1.getDenominator() + "=");
        return op1.toString();
    }
    public static IntNumber obj(){  //随机生成一个整数
        Random ran = new Random();
        return new IntNumber(ran.nextInt(100));
    }
}
