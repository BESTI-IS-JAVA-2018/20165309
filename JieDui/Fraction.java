import java.util.Random;
public class Fraction { //除法加减乘除
    private int numerator, denominator; //分别是分子、分母
    public  Fraction (int numer, int denom) { //写成一个分数形式
        if(denom == 0 )   //分母为零
            denom = 1;
        if (denom < 0) {  //分母为负数
            numer = numer * -1;
            denom = denom * -1;
        }
        numerator = numer;
        denominator = denom;
        reduce();
    }
    public int getNumerator() {
        return numerator;
    }
    public int getDenominator() {
        return denominator;
    }
    public Fraction add(Fraction op2) { //加法
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;
        System.out.print("("+this.toString()+")" + " + " + "("+op2.toString()+")" + "=");
        return new Fraction (sum, commonDenominator);
    }
    public Fraction subtract(Fraction op2) { //减法
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 - numerator2;
        System.out.print("("+this.toString()+")" + " - " + "("+op2.toString()+")" + "=");
        return new Fraction(difference,commonDenominator);
    }
    public Fraction multiply (Fraction op2) { //乘法
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();
        System.out.print("("+this.toString()+")" + " * " + "("+op2.toString()+")" + "=");
        return new Fraction (numer, denom);
    }
    public Fraction divide (Fraction op2) { //除法
        int numer = numerator * op2.getDenominator();
        int denom = denominator * op2.getNumerator();
        System.out.print("("+this.toString()+")" + " / " + "("+op2.toString()+")" + "=");
        return new Fraction (numer, denom);
    }
    public String toString() {
        String result;
        if (numerator == 0)
            result = "0";
        else if(denominator == 0)
            return "错误！分母不能为0";
        else if (denominator == 1)
            result = numerator + "";
        else
            result = numerator + "/" + denominator;
        return result;
    }
    private void reduce() {  //约去公因子
        if (numerator != 0) {
            int common = gcd (Math.abs(numerator), denominator);
            numerator = numerator / common;
            denominator = denominator / common;
        }
    }
    private int gcd (int num1, int num2) { //最大公因子
        if(num2==0)
            return num1;
        else
            return gcd(num2,num1%num2);
    }
    public static Fraction obj(){  //随机生成一个真分数的分子和分母
        Random ran = new Random();
        return new Fraction(ran.nextInt(100),ran.nextInt(100));
    }
}
