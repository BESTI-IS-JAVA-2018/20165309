import java.text.NumberFormat;
import java.util.*;
public class Calculate {
    public static void main(String[] args) {
        NumberFormat number = NumberFormat.getPercentInstance();
        Random ran = new Random();
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.print("请输入您所需要的题目数：");
            int A = scan.nextInt();    //A;题目数量
            for (int i = 0; i < A; i++) {
                int B = ran.nextInt(2);  //生成随机数0/1，随机生成整数或分数
                int C = ran.nextInt(4); //生成随机数0-3，随机生成4种运算
                IntNumber in1 = IntNumber.obj();
                IntNumber in2 = IntNumber.obj();
                Fraction score1 = Fraction.obj();  //分数运算
                Fraction score2 = Fraction.obj();
                if (B == 0) {
                    switch (C) {     //整数加减乘除运算
                        case 0:
                            int num = in1.add(in2);
                            String num1 = "" + num;
                            int n = scan.nextInt();
                            Judgement.judge(n == num, num1);
                            break;
                        case 1:																																																					                            num = in1.subtract(in2);																																																								                                num1 = "" + num;
                            n = scan.nextInt();
                            Judgement.judge(n == num, num1);
                            break;
                        case 2:
                            num = in1.multiply(in2);
                            num1 = "" + num;
                            n = scan.nextInt();
                            Judgement.judge(n == num, num1);
                            break;
                        case 3:
                            num1 = in1.divide(score1);
                            String Q = scan.next();
                            Judgement.judge(Q.equals(num1), num1);
                            break;
                    }
                } else {
                    switch (C) {     //分数加减乘除运算
                        case 0:
                            Fraction num2 = score1.add(score2);
                            String num1 = num2.toString();
                            String s = scan.next();
                            Judgement.judge(s.equals(num1), num1);
                            break;
                        case 1:
                            num2 = score1.subtract(score2);
                            num1 = num2.toString();
                            s = scan.next();
                            Judgement.judge(s.equals(num1), num1);
                            break;
                        case 2:
                            num2 = score1.multiply(score2);
                            num1 = num2.toString();
                            s = scan.next();
                            Judgement.judge(s.equals(num1), num1);
                            break;
                        case 3:
                            num2 = score1.divide(score2);
                            num1 = num2.toString();
                            s = scan.next();
                            Judgement.judge(s.equals(num1), num1);
                            break;
                    }
                }
            }
            System.out.println("你答对的题目总数：" +  Judgement.getTrues());
            double T = (double) Judgement.getTrues() / A;
            System.out.println("您的正确率为:" + number.format(T));
            System.out.println("是否继续生成题目？（y/n）:"  );
            String s1 = scan.next();
            if (s1.equalsIgnoreCase("n")){
                break;
            }
        }
    }
}

