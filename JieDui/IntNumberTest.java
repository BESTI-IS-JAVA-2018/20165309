import junit.framework.TestCase;
public class IntNumberTest extends TestCase {

    public void testAdd () {
        assertEquals(25, 20 + 5);
        assertEquals(-3,-12 + 9);
        assertEquals(-7,-2 + (-5));
    }

    public void testSubtract () {
        assertEquals(14, 24 - 10);
        assertEquals(-3,14 - 17);
        assertEquals(-12,0 - 12);
    }

    public void testMultiply () {
        assertEquals(100, 25 * 4);
        assertEquals(0,0 * 0);
        assertEquals(-100,25 * (-4));
    }

    public void testDivide () {
        assertEquals(10, 100 / 10);
        assertEquals(0,0 / 10);
        assertEquals(-10,100/ (-10));
    }


}

