import junit.framework.TestCase;

public class FractionTest extends TestCase {
    public void testAdd() {
        assertEquals(1/15, 2/5+(-1/3));
        assertEquals(0, 1 / 7 + (-1 / 7));
        assertEquals(-29 / 45, -1 / 5 + (-4 / 9));
    }

    public void testSubtract() {
        assertEquals(4 / 9, 7 / 9 - 1 / 3);
        assertEquals(0, 6 / 11 - 6 / 11);
        assertEquals(-5 / 9, -7 / 9 - (-2 / 9));
    }

    public void testMultiply() {
        assertEquals(4 / 27, 2 / 9 * 2 / 3);
        assertEquals(0, 1 * 0 / 3);
        assertEquals(-2 / 3, 1 / 3 * (-2));
    }

    public void testDivide() {
        assertEquals(6 / 17, 12 / 17 / 1 / 2);
        assertEquals(0, 0 / 12 / 17);
        assertEquals(-1 / 17, -1 / 17);
    }

}
