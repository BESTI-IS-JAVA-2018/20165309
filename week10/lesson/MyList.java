import java.util.*;

public class MyList {
    public static void main(String[] args) {
        List<String> list = new LinkedList<String>();
        list.add("20165307");
        list.add("20165308");
        list.add("20165310");
        list.add("20165311");
        System.out.println("当前链表中的数据为：");
        Iterator<String> iter = list.iterator();
        while (iter.hasNext()) {
            String te = iter.next();
            System.out.println(te);
        }
        list.add("20165309");
        System.out.println("插入我的学号，排序后打印链表：");
        Collections.sort(list);
        iter = list.iterator();
        while (iter.hasNext()) {
            String te = iter.next();
            System.out.println(te);
        }
        list.remove("20165309");
        System.out.println("删除我的学号后打印链表：");
        iter = list.iterator();
        while (iter.hasNext()) {
            String te = iter.next();
            System.out.println(te);
        }
    }
}


