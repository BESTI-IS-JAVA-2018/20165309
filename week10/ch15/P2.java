import java.util.*;

class CollegeStu implements Comparable {
    int english = 0;
    String name;

    CollegeStu(int english, String name) {
        this.name = name;
        this.english = english;
    }

    @Override
    public int compareTo(Object b) {
        CollegeStu stu = (CollegeStu) b;
        return (this.english - stu.english);
    }
}

public class P2 {
    public static void main(String[] args) {
        List<CollegeStu> list = new LinkedList<CollegeStu>();
        int score[] = {59, 12, 99, 74, 66};
        String name[] = {"aa", "bb", "me", "cc", "dd"};
        for (int i = 0; i < score.length; i++) {
            list.add(new CollegeStu(score[i], name[i]));
        }
        Iterator<CollegeStu> iter = list.iterator();
        TreeSet<CollegeStu> mytree = new TreeSet<CollegeStu>();
        while (iter.hasNext()) {
            CollegeStu stu = iter.next();
            mytree.add(stu);
        }
        Iterator<CollegeStu> te = mytree.iterator();
        while (te.hasNext()) {
            CollegeStu stu = te.next();
            System.out.println("" + stu.name + " " + stu.english);
        }
    }
}
