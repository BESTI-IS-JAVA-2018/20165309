import java.util.*;

public class P1 {
    public static void main(String args[]) {
        Stack<Integer> stack = new Stack<Integer>();//建立一个堆栈对象
        stack.push(8);
        stack.push(3);
        int k = 1;
        while (k <= 10) {
            for (int i = 1; i <= 2; i++) {
                int f1 = stack.pop();
                int f2 = stack.pop();
                Integer temp = 2 * (f1 + f2);
                System.out.println("" + temp.toString());
                stack.push(temp);
                stack.push(f2);
                k++;
            }
        }
    }
}
