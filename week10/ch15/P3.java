import java.util.*;

class UDiscKey implements Comparable {
    double key = 0;

    UDiscKey(double d) {
        key = d;
    }

    @Override
    public int compareTo(Object b) {
        UDiscKey disc = (UDiscKey) b;
        if ((this.key - disc.key) == 0) {
            return -1;
        } else {
            return (int) ((this.key - disc.key) * 1000);
        }
    }
}

class UDisc {
    int amount;
    double price;

    UDisc(int m, double e) {
        amount = m;
        price = e;
    }
}

public class P3 {
    public static void main(String[] args) {
        TreeMap<UDiscKey, UDisc> treeMap = new TreeMap<UDiscKey, UDisc>();
        int amount[] = {1, 2, 8, 16, 32, 54, 9, 77, 98, 30};
        double price[] = {20, 35, 666, 999, 333, 56, 43, 887, 12, 76};
        UDisc UDisc[] = new UDisc[10];
        for (int k = 0; k < UDisc.length; k++) {
            UDisc[k] = new UDisc(amount[k], price[k]);
        }
        UDiscKey key[] = new UDiscKey[10];
        for (int k = 0; k < key.length; k++) {
            key[k] = new UDiscKey(UDisc[k].amount);
        }
        for (int k = 0; k < UDisc.length; k++) {
            treeMap.put(key[k], UDisc[k]);
        }
        int number = treeMap.size();
        Collection<UDisc> collection = treeMap.values();
        Iterator<UDisc> iter = collection.iterator();
        while (iter.hasNext()) {
            UDisc disc = iter.next();
            System.out.println("" + disc.amount + "G " + disc.price + "元");
        }
        treeMap.clear();
        for (int k = 0; k < key.length; k++) {
            key[k] = new UDiscKey(UDisc[k].price);
        }
        for (int k = 0; k < UDisc.length; k++) {
            treeMap.put(key[k], UDisc[k]);
        }
        number = treeMap.size();
        collection = treeMap.values();
        iter = collection.iterator();
        while (iter.hasNext()) {
            UDisc disc = iter.next();
            System.out.println("" + disc.amount + "G " + disc.price + "元");
        }
    }
}
