import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.binarySearch;
import static org.junit.Assert.*;

/**
 * Created by wsj on 2018/5/20.
 */
public class codeTest extends TestCase {

    StringBuffer string1 = new StringBuffer("Beautiful");
    StringBuffer string2 = new StringBuffer("Beautiful Girls");
    StringBuffer string3 = new StringBuffer("Beautiful Girls and Boys");



    @Test
    public void testCharAt(){
        assertEquals('a',string1.charAt(2));
        assertEquals(' ',string2.charAt(9));
        assertEquals('a',string3.charAt(16));
    }
    @Test
    public void testCapacity(){
        assertEquals(25,string1.capacity());
        assertEquals(31,string2.capacity());
        assertEquals(40,string3.capacity());
    }
    @Test
    public void testindexOf() {
        assertEquals(1, string3.indexOf("ea"));
    }
    @Test
    public void testlength() {
        assertEquals(9, string1.length());
    }
    @Test
    public void StringTester() {
        String string="aa:30:3:5";
        String[] strings=string.split(":");
        assertEquals(':',string.charAt(2));
        assertEquals("30",strings[1]);
    }
    @Test
    public void ArraysTester() {
        int[] arr={1,2,5,4};
        Arrays.sort(arr);
        assertEquals(5,arr[3]);
        assertEquals(1,binarySearch(arr,2));
    }

}
