public class Dog {
    private String color;
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public String bark(){
        return "汪汪";
    }
    public String toString(){
        return "The Dog's color is " + this.getColor() +", and it shouts "+ this.bark() + "!";
    }
}
