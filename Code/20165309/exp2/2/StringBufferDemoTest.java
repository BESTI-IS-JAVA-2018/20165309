import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringBufferDemoTest extends TestCase {

    StringBuffer string1 = new StringBuffer("Beautiful");
    StringBuffer string2 = new StringBuffer("Beautiful Girls");
    StringBuffer string3 = new StringBuffer("Beautiful Girls and Boys");
    @Test
    public void testCharAt(){
        assertEquals('a',string1.charAt(2));
        assertEquals(' ',string2.charAt(9));
        assertEquals('a',string3.charAt(16));
    }
    @Test
    public void testCapacity(){
        assertEquals(25,string1.capacity());
        assertEquals(31,string2.capacity());
        assertEquals(40,string3.capacity());
    }
    @Test
    public void testindexOf() {
        assertEquals(1, string3.indexOf("ea"));
    }
    @Test
    public void testlength() {
        assertEquals(9, string1.length());
    }
}

