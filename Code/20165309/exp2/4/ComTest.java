import junit.framework.TestCase;
import org.junit.Test;
public class ComTest extends TestCase {
	Com c1 = new Com(1,1);
	Com c2 = new Com(0,-1);
	Com c3 = new Com(-1,1);
	@Test
	public void testgetRealPart() throws Exception {
		assertEquals(1.1, new Com().setA(1.1));
		assertEquals(0.-1, new Com().setA(0.-1));
		assertEquals(-1.1, new Com().setA(-1.1));
	}
	@Test
	public void testgetImagePart() throws Exception {
		assertEquals(1.1, new Com().setB(1.1));
		assertEquals(0.-1, new Com().setB(0.-1));
		assertEquals(-1.1, new Com().setB(-1.1));
	}
	@Test
	public void testComAdd() throws Exception {
		assertEquals("1.0", c1.ComAdd(c2).toString());
		assertEquals("0.0+2.0i", c1.ComAdd(c3).toString());
		assertEquals("-1.0", c2.ComAdd(c3).toString());
	}
	@Test
	public void testComSub() throws Exception {
		assertEquals("-1.0 -2.0i", c1.ComMinus(c2).toString());
		assertEquals("-2.0", c1.ComMinus(c3).toString());
		assertEquals("-1.0+2.0i", c2.ComMinus(c3).toString());
	}
}
