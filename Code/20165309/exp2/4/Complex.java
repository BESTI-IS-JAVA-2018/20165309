public class Complex {    //a + bi
    private double a;
    private double b;

    public Complex(){                      //构造方法，置0
        this.a = 0;
        this.b = 0;
    }

    public Complex(double a, double b) {  //构造方法，初始化一个复数
        this.a = a;
        this.b = b;
    }

    public  double getA(){                  //获取实部
        return this.a;
    }
    public double getB(){                  //获取虚部
        return this.b;
    }

    public double setA(double a){         //设置实部
        this.a = a;
        return a;
    }
    public double setB(double b){         //设置虚部
        this.b = b;
        return b;
    }

    Complex ComplexAdd(Complex c){//复数相加
        double a = c.getA();
        double b = c.getB();
        double newA = a + this.a;
        double newB = b + this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }

    Complex ComplexMinus(Complex c){//复数相减
        double a = c.getA();
        double b = c.getB();
        double newA = a - this.a;
        double newB = b - this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }

    Complex ComplexMulti(Complex c){//复数相乘
        double a = c.getA();
        double b = c.getB();
        double newA = a * this.a;
        double newB = b * this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }

    Complex ComplexDiv(Complex c){//复数相乘
        double a = c.getA();
        double b = c.getB();
        double newA = a / this.a;
        double newB = b / this.b;
        Complex Result = new Complex(newA,newB);
        return Result;
    }

    public String toString() {
        String s = " ";
        if (b > 0)
            s =  a + "+" + b + "i";
        if (b == 0)
            s =  a + "";
        if (b < 0)
            s = a + " " + b + "i";
        return s;
    }
}
