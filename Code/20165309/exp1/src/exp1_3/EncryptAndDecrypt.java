class EncryptAndDecrypt {
    String encrypt(String sourceString,int password) throws EncryptAndDecryptException { //加密算法
        char [] c = sourceString.toCharArray();
        int m = c.length;
        for(int k=0;k<m;k++){
            if((c[k]>=65&&c[k]<=90)||(c[k]>=97&&c[k]<=122)){
                int mima=(c[k] -'a' + password) % 26 + 'a';       //加密,同时转码
                c[k]=(char)mima;
            }
            else {
                throw new EncryptAndDecryptException(sourceString);
            }
        }
        return new String(c);    //返回密文
    }
    String decrypt(String sourceString,int password) throws EncryptAndDecryptException { //解密算法
        char [] c = sourceString.toCharArray();
        int m = c.length;
        for(int k=0;k<m;k++){
            if((c[k]>=65&&c[k]<=90)||(c[k]>=97&&c[k]<=122)){
                int mima=((c[k] + 26) -'a' - password) % 26 + 'a';       //解密,同时转码
                c[k]=(char)mima;
            }
            else {
                throw new EncryptAndDecryptException(sourceString);
            }
        }
        return new String(c);    //返回明文
    }
}
