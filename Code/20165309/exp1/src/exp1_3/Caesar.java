import java.io.*;
import java.util.*;
public class Caesar {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Hi，这里是凯撒密码计算^_^!");
        System.out.println("你想进行运算吗？输入一个非0数字继续，输入0即为退出哦~");
        int a =in.nextInt();
        while(a!=0) {
            System.out.println("1.加密  2.解密");//要执行的操作
            System.out.println("选一个要进行的操作吧：");
            int n = in.nextInt();
            if(n == 1) {
                try {
                    EncryptAndDecrypt person = new EncryptAndDecrypt();
                    System.out.print("明文：");
                    Scanner scan = new Scanner(System.in);
                    String sourceString = scan.next();
                    System.out.println("输入加密偏移量:");
                    Scanner scanner = new Scanner(System.in);
                    int password = scanner.nextInt();
                    String secret = person.encrypt(sourceString,password);
                    System.out.println("密文:"+secret);
                }
                catch(EncryptAndDecryptException e) {
                    System.out.println("Warning:");
                    System.out.println(e.warnMess());
                }
                System.out.println("你还想继续吗？");
                int num = in.nextInt();
                if(num == 0) {
                    System.out.println("Thanks,bye~");
                    System.exit(0);
                }
            }
            else if(n == 2) {
                try {
                    EncryptAndDecrypt person = new EncryptAndDecrypt();
                    System.out.println("密文:");
                    Scanner scan = new Scanner(System.in);
                    String sourceString = scan.next();
                    System.out.println("输入解密偏移量：");
                    Scanner scanner = new Scanner(System.in);
                    int password = scanner.nextInt();
                    String source = person.decrypt(sourceString,password);
                    System.out.println("明文:"+source);
                }
                catch(EncryptAndDecryptException e) {
                    System.out.println("Warning:");
                    System.out.println(e.warnMess());
                }
                System.out.println("你还想继续吗？");
                int num = in.nextInt();
                if(num == 0) {
                    System.out.println("Thanks,bye~");
                    System.exit(0);
                }
            }
        }
        if(a == 0) {
            System.out.println("Thanks,bye~");
        }
    }
}
