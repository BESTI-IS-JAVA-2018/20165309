class studentdark {
	private int num;
	private String name;
	private int java;
	public int getNum() {
		return num;
	}
	@Override
	public String toString() {
		return "studentdark{" +
			"num=" + num +
			", name='" + name + '\'' +
			", java=" + java +
			'}';
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getJava() {
		return java;
	}
	public void setJava(int java) {
		this.java = java;
	}
}
public class studentdarkTest {
    public static void main(String[] args) {
	    studentdark student = new studentdark();
	    student.setName("caoge");
	    student.setNum(5312);
	    student.setJava(94);
	    System.out.println("学号为"+ student.getNum() +",姓名为"+ student.getName() +"java成绩为"+ student.getJava());
    }

}


