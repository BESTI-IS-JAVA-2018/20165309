class Student {//存放学生的信息
	private int num;
	private String name;
	private int java;
	private int math;
	private int English;
	private int sum;
	private int avg;
	public void setNum(int num) {
		this.num = num;
	}
	public int getNum() {
		return num;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setJava(int java) {
		this.java = java;
	}
	public int getJava() {
		return java;
	}
	public void setmath(int math) {
		this.math = math;
	}
	public int getmath() {
		return math;
	}
	public void setEnglish(int English) {
		this.English=English;
	}
	public int getEnglish() {
		return English;
	}
	public void setSum() {
		this.sum = this.java+this.math+this.English;
	}
	public int getSum() {
		return sum;
	}
	public void setAvg() {
		this.avg = this.sum/3;
	}
	public int getAvg() {
		return avg;
	}
	public String toString(){
		String str = "\t"+  this.num+"\t"+  this.name+"\t"+"\t"	+  this.java+"\t"+  this.math +"\t"+this.English+"\t"+  this.sum+"\t"+  this.avg;
		return str;
	}
}
