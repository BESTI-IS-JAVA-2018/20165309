import java.net.*;
import java.io.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.spec.*;
import javax.crypto.interfaces.*;
import java.security.interfaces.*;
import java.math.*;

public class Client {
    public static void main(String srgs[]) throws Exception {
        try {
            KeyGenerator kg = KeyGenerator.getInstance("DESede");
            kg.init(168);
            SecretKey k = kg.generateKey();
            byte[] ptext2 = k.getEncoded();
            Socket socket = new Socket("127.0.0.1", 10000);
            System.out.println("客户端成功启动，等待客户端呼叫");
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
            //RSA算法，使用服务器端的公钥对DES的密钥进行加密
            FileInputStream f3 = new FileInputStream("Skey_RSA_pub.dat");
            ObjectInputStream b2 = new ObjectInputStream(f3);
            RSAPublicKey pbk = (RSAPublicKey) b2.readObject();
            BigInteger e = pbk.getPublicExponent();
            BigInteger n = pbk.getModulus();
            BigInteger m = new BigInteger(ptext2);
            BigInteger c = m.modPow(e, n);
            String cs = c.toString();
            out.println(cs); // 通过网络将加密后的秘钥传送到服务器
            System.out.println("请输入中缀表达式：");
            //用DES加密明文得到密文
            String s = stdin.readLine(); // 从键盘读入待发送的数据
            String postfix = MyBC.toPostfix(s);
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, k);
            byte ptext[] = postfix.getBytes("UTF8");
            byte ctext[] = cp.doFinal(ptext);
            String str = parseByte2HexStr(ctext);
            out.println(str); // 通过网络将密文传送到服务器

            // 将客户端明文的Hash值传送给服务器
            String x = postfix;
            MessageDigest m2 = MessageDigest.getInstance("MD5");
            m2.update(x.getBytes());
            byte a[] = m2.digest();
            String result = "";
            for (int i = 0; i < a.length; i++) {
                result += Integer.toHexString((0x000000ff & a[i]) | 0xffffff00).substring(6);
            }
            System.out.println("MD5:"+result);
            out.println(result);//通过网络将明文的Hash函数值传送到服务器
            str = in.readLine();// 从网络输入流读取结果
            System.out.println("从服务器接收到的结果为：" + str); // 输出服务器返回的结果
        } catch (Exception e) {
            System.out.println(e);//输出异常
        } finally {
        }

    }

    //将十六进制转换成二进制
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }
}