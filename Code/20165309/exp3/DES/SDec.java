import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;
import javax.crypto.spec.*;

/**
 * @author W and C
 * @date 2018/4/28
 */
public class SDec {
    public static void main(String args[]) throws Exception {
        // 获取密文
        FileInputStream f = new FileInputStream("SEnc.dat");
        int num = f.available();
        byte[] ctext = new byte[num];
        f.read(ctext);
        // 获取密钥
        FileInputStream f2 = new FileInputStream("keykb1.dat");
        int num2 = f2.available();
        byte[] keykb = new byte[num2];
        f2.read(keykb);
        SecretKeySpec k = new SecretKeySpec(keykb, "DESede");
        // 解密
        byte[] ptext = getBytes(ctext, k);
        // 显示明文
        Exist(ptext);
    }

    private static void Exist(byte[] ptext) throws UnsupportedEncodingException {
        String p = new String(ptext, "UTF8");
        System.out.println(p);
    }

    private static byte[] getBytes(byte[] ctext, SecretKeySpec k) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cp = Cipher.getInstance("DESede");
        cp.init(Cipher.DECRYPT_MODE, k);
        return cp.doFinal(ctext);
    }
}
