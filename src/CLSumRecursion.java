 class CLSumRecursion {
        public static void main(String args[]) {
                int sum = 0;
                if(args.length < 1) {
                        System.out.println("Usage:java CLSumRecursion num1 num2 ...");
                        System.exit(0);
                }
                int tmp[] = new int[args.length];
                for(int i=0;i<args.length;i++) {
                        tmp[i] = Integer.parseInt(args[i]);
                }
                if(tmp[0]<=0) {
                        System.out.println("Please check your input!");
                        System.exit(0);
                }
                else {
                        for(int j=1;j<=tmp[0];j++) {
                        sum += fact(j);
                        }
                }
                if(sum<=0) {
                        System.out.println("Please check your input!");
                        System.exit(0);
                }
                else
                        System.out.println(sum);
        }
        public static int fact(int n) {
                if(n == 0)
                        return 1;
                else
                        return n*fact(n-1);
        }
}
