public class Test {
	public static void main(String args[]) {
		CPU cpu = new CPU(2200);
		HardDisk HD = new HardDisk(200);
		PC pc = new PC(cpu,HD);
		String s = new String();
		pc.show();
		s = cpu.toString();
		System.out.println(s);
		s = HD.toString();
		System.out.println(s);
		s = pc.toString();
		System.out.println(s);
		boolean b;
		Object virtualPC = new Object();
		virtualPC = pc;
		b = pc.equals(virtualPC);
		System.out.println(b);
		virtualPC = cpu;
		b = cpu.equals(virtualPC);
		System.out.println(b);
		virtualPC = HD;
		b = HD.equals(virtualPC);
		System.out.println(b);
	}
}

