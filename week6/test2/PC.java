public class PC extends Object{
	CPU cpu;
	HardDisk HD;
	PC() {}
	PC(CPU cpu) {
		this.cpu = cpu;
	}
	PC(HardDisk HD) {
		this.HD = HD;
	}
	PC(CPU cpu,HardDisk HD) {
		this.cpu = cpu;
		this.HD = HD;
	}
	public void setCPU(CPU a) {
		cpu = a;
	}
	public void setHardDisk(HardDisk b) {
		HD = b;
	}
	public void show() {
		System.out.printf("CPU速度为: "+cpu.getSpeed()+"，磁盘容量为: "+HD.getAmount());
	}
	public String toString() {
		return "PC已覆盖toString";
	}
	public boolean equals(Object obj) {
		return true;
	}
}
