import java.util.*;
public class Combination {
    public static void main(String[] args) {
	int Com;
	if(args.length < 1){
            System.out.println("Usage: java Combination num1 num2");
            System.exit(0);
        }
 
        int [] tmp = new int [args.length];
        for(int i=0; i<args.length; i++) {
            tmp[i] = Integer.parseInt(args[i]);
        }
	Com = fact(tmp[0],tmp[1]);
        if (Com == 0) {
            System.out.println("Wrong!");
        }
        else {
            System.out.println(Com);
        }
    }

    public static int fact(int a, int b) {
        if (a < b || a < 0 || b < 0) {
            return 0;
        } else if (b == a || b == 0) {
            return 1;
        } else {
            return fact(a - 1, b - 1) + fact(a - 1, b);
        }
    }
}
